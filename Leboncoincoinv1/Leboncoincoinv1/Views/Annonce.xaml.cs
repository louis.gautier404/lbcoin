﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Leboncoincoinv1.Model;
using Leboncoincoinv1;

namespace Leboncoincoinv1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Annonce : ContentPage
    {
        
        public Annonce()
        {
            
            InitializeComponent();
            listeAnnonces.ItemsSource = DatasAnnonces.GetAnnonces();
            

        }


        private async void ListeAnnonces_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var index = e.ItemIndex;
            var annonces = DatasAnnonces.GetAnnonces();
            var id = annonces[index].ID;
            //var bd = (ID)sender;
            //var button = (Button)sender;
            //var id = Convert.ToInt32(button.Text);
            await Navigation.PushAsync(new AnnonceEnSavoirPlus(id));
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Leboncoincoinv1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnnonceEnSavoirPlus : ContentPage
    {
        public AnnonceEnSavoirPlus(int id)
        {
            
            InitializeComponent();
            var CallUsLabel = new Label { Text = "Tap or click here to call" };
            CallUsLabel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => {
                    Device.OpenUri(new Uri("tel:038773729"));
                })
            });

            
            //int id = 0;
            listeAnnoncesDetail.ItemsSource = DatasAnnonces.GetAnnoncesDetail(id);

           
        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leboncoincoinv1.Model
{
   public class Annonce
    {
        public int ID { get; set; }

        public string Titre { get; set; }

        public string Description { get; set; }

        public int Prix { get; set; }

        public int NuméroDeTelephone { get; set; }

        public string Categorie { get; set; }

    }
}

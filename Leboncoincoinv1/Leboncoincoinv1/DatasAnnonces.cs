﻿using System;
using System.Collections.Generic;
using System.Text;
using Leboncoincoinv1.Model;
namespace Leboncoincoinv1
{
    public class DatasAnnonces
    {
        public static List<Annonce> GetAnnonces()
        {
            Annonce A1 = new Annonce();

            A1.ID = 0;
            A1.Titre = "Test";
            A1.Description = "test";
            A1.Prix = 0;
            A1.NuméroDeTelephone = 0251094157;


            Annonce A2 = new Annonce();

            A2.ID = 1;
            A2.Titre = "Test2";
            A2.Description = "test2";
            A2.Prix = 2;
            A2.NuméroDeTelephone = 77387878;

            List<Annonce> annonces = new List<Annonce>();
            annonces.Add(A1);
            annonces.Add(A2);

            return annonces;
        }

        public static List<Annonce> GetAnnoncesDetail(int id)
        {
           
            List<Annonce> annoncesDetail = new List<Annonce>();
            foreach (var item in GetAnnonces())
            {
                if(item.ID == id)
                {
                    annoncesDetail.Add(item);
                }
            }
           


            return annoncesDetail;
        }




    }
}
